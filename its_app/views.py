from django.shortcuts import render
from django.shortcuts import render
from scipy.spatial import distance
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from . import models, serializers
import statistics as stat
from datetime import datetime,timedelta
import random
# Create your views here.

def DetermineClass(lat,lon):
    if lat>=58.375 and lat <=58.385 and lon>=26.725 and lon<=26.735:
        return "a"

    elif lat>=58.375 and lat <=58.385 and lon>=26.715 and lon<=26.725:
        return "b"

    elif lat>=58.365 and lat <=58.375 and lon>=26.715 and lon<=26.740:
        return "c"

    else:
        return "d"

def ClusterListGenerator(data):

    hash_data = dict()
    for item in data :
        hash_data[item]=list()
    for item1 in data :
        for item2 in data :
            if item1 != item2:
                x1 = float(item1.split("$")[0])
                y1 = float(item1.split("$")[1])
                x2 = float(item2.split("$")[0])
                y2 = float(item2.split("$")[1])

                if distance.euclidean((x1,y1),(x2,y2)) < 0.0085 :
                    hash_data[item1].append(item2)

    counter_hash=dict()


    for records in hash_data.values():
        for rec in records:
            if rec not in counter_hash.keys():
                counter_hash[rec] = 1
            else :
                counter_hash[rec] += 1
    final_list=list()
    for key in counter_hash.keys():
        if counter_hash[key] > 2 :
            final_list.append(key)


    return final_list


class test_receiver(APIView):
    def get(self,request):
        # cells = models.Cell.objects.all()
        # for cell1 in cells:
        #     for cell2 in cells:
        #         if cell1.lon == cell2.lon and cell1.lat == cell2.lat and cell1.cell_id != cell2.cell_id:
        #             print("===============")
        #             print(cell1.lat)
        #             print(cell1.lon)
        #             cell2.lat += random.uniform(-0.01, 0.01)
        #             cell2.lon += random.uniform(-0.01, 0.01)
        #             cell2.save()


        #-------------------------------------------------

        # cells = models.Cell.objects.all()
        # for cell in cells:
        #     if cell.cell_id != 11111:
        #         cell.delete()
        #
        # f = open("cells_coordinates.txt", "r")
        # cells = f.readlines()
        # for cell in cells:
        #     cell_data = cell.split(";")
        #     cell_entity = models.Cell(cell_id=int(cell_data[0]), lat=float(cell_data[1]), lon=float(cell_data[2]))
        #     cell_entity.save()
        #     print(cell_entity.cell_id)
        # f.close()
        # del (cells)

        #----------------------------------------------
        # begin_date = datetime(year=2019,month=6,day=1)
        # end_date = datetime(year=2019,month=8,day=30)
        # current_date = begin_date
        # cells = models.Cell.objects.all()
        # while True :
        #     for cell in cells:
        #         print("{} --- {}".format(current_date.__str__(),cell.cell_id.__str__()))
        #         area = DetermineClass(cell.lat,cell.lon)
        #         if area == "a":
        #             range_number = random.randint(10,20)
        #             for i in range(range_number):
        #                 record = models.Data(cell_id=cell,pos_usr_id=random.randint(1999999,8999999),pos_time=current_date)
        #                 record.save()
        #
        #         if area == "b":
        #             range_number = random.randint(6, 12)
        #             for i in range(range_number):
        #                 record = models.Data(cell_id=cell,pos_usr_id=random.randint(1999999,8999999),pos_time=current_date)
        #                 record.save()
        #
        #         if area == "c":
        #             range_number = random.randint(3, 8)
        #             for i in range(range_number):
        #                 record = models.Data(cell_id=cell,pos_usr_id=random.randint(1999999,8999999),pos_time=current_date)
        #                 record.save()
        #
        #         if area == "d":
        #             range_number = random.randint(1, 4)
        #             for i in range(range_number):
        #                 record = models.Data(cell_id=cell,pos_usr_id=random.randint(1999999,8999999),pos_time=current_date)
        #                 record.save()
        #
        #     current_date += timedelta(days=1)
        #     if current_date > end_date:
        #         break

        date1 = datetime(year=2019,month=7,day=17)
        date2 = datetime(year=2019, month=7, day=18)
        date3 = datetime(year=2019, month=7, day=19)

        cells = models.Cell.objects.all()
        for cell in cells :
            if cell.lat>=58.39 and cell.lat <=58.42 and cell.lon>=26.75 and cell.lon<=26.80:
                for i in range(300):
                    data=models.Data(cell_id=cell,pos_usr_id=random.randint(188888,9999999),pos_time=date1)
                    data.save()
                    print("data 1")
                for i in range(500):
                    data=models.Data(cell_id=cell, pos_usr_id=random.randint(188888, 9999999), pos_time=date2)
                    data.save()
                    print("data 2")
                for i in range(200):
                    data=models.Data(cell_id=cell, pos_usr_id=random.randint(188888, 9999999), pos_time=date3)
                    data.save()
                    print("data 3")

        # models.Data.objects.all().delete()
        return Response({"message": " the server is up and running!"})

class Initialize(APIView):
    def get(self,request):



        cells = models.Cell.objects.all()

        for cell in cells :
            data_all = models.Data.objects.filter(cell_id=cell).order_by('pos_time')
            # print(len(data_all))
            last_date = data_all[len(data_all)-1].pos_time
            weeks = list()
            
            starter_date = data_all[0].pos_time

            while True :
                end_week_day = starter_date + timedelta(days=7)
                num = len(models.Data.objects.filter(cell_id=cell,pos_time__gte=starter_date,pos_time__lt=end_week_day))
                weeks.append(num)
                if end_week_day > last_date :
                    break

                starter_date = end_week_day


            print(weeks)
            std_dev = stat.stdev(weeks)
            avg = stat.mean(weeks)
            print("average: {}".format(avg))
            print("std: {}".format(std_dev))

            try :
                statistical = models.StatisticalOverView.objects.get(cell=cell)
                statistical.delete()
            except:
                pass

            statistical = models.StatisticalOverView(cell=cell, std_dev1=std_dev, mean=avg)


            statistical.save()


        return Response({"message":"ok"})





class Monitor(APIView):
    def get(self,request):
        pass

    def post(self,request):

        cells = models.Cell.objects.all()
        response_list = list()


        for cell in cells :
            cell_data = models.Data.objects.filter(cell_id=cell,
                                                       pos_time__range=[request.data["from_date"],request.data["to_date"]]).order_by("pos_time")
            if len(cell_data)==0:
                continue
            last_date = cell_data[len(cell_data) - 1].pos_time
            weeks = list()

            starter_date = cell_data[0].pos_time

            while True:
                end_week_day = starter_date + timedelta(days=7)
                num = len(models.Data.objects.filter(cell_id=cell,pos_time__gte=starter_date, pos_time__lt=end_week_day))
                weeks.append(num)
                if end_week_day > last_date:
                    break

                starter_date = end_week_day



            cell_result = dict()
            cell_result["cell_id"]=cell.cell_id
            # in  2 ta moteqaiero baadan avaz kon hatman tenqe azmaiesh
            unusual_coefficient = 1.5
            extereme_coefficient = 3
            week_results = list()
            print(cell.cell_id)
            stats_review = models.StatisticalOverView.objects.get(cell=cell)


            for week in weeks:
                #0 : normal
                #1 : high
                #2 : extereme
                final_result_level = 0
                if week>(stats_review.mean + (stats_review.std_dev1*extereme_coefficient)):
                    final_result_level = 2
                elif week>(stats_review.mean + (stats_review.std_dev1*unusual_coefficient)):
                    final_result_level = 1

                week_results.append(final_result_level)



            cell_result["cell_id"]=cell.cell_id
            cell_result["weeks"]=week_results
            cell_result["lat"]=cell.lat
            cell_result["lon"]=cell.lon
            response_list.append(cell_result)


        # clustering section ============================
        num_of_weeks = len(response_list[0]["weeks"])
        weeks_busy_cells_coor = list()
        for i in range(num_of_weeks):
            cells_coor_busy = list()
            for item in response_list:
                if item["weeks"][i] == 2 :
                    cells_coor_busy.append("{}${}".format(item["lat"],item["lon"]))
            weeks_busy_cells_coor.append(cells_coor_busy)

        print(weeks_busy_cells_coor)
        week_index = 0
        week_busy_dict = dict()
        for cell_coor_list in weeks_busy_cells_coor:
            busy_cells_coors = ClusterListGenerator(cell_coor_list)
            week_busy_dict[week_index] = busy_cells_coors
            week_index+=1

        response_busy_coor = list()

        for key in week_busy_dict:
            for item in week_busy_dict[key]:
                lat = float(item.split("$")[0])
                lon = float(item.split("$")[1])
                latlon = {"lat": lat, "lon": lon,"week_index":key}
                response_busy_coor.append(latlon)

        print(response_busy_coor)

        # print("----")
        # print(len(models.Data.objects.filter(cell_id=models.Cell.objects.get(cell_id=7951))))
        cells_data = models.Cell.objects.all()
        serial = serializers.CellSerializers(cells_data,many=True)
        return Response({"data":response_list,"busy_coor":response_busy_coor,"cells":serial.data},status=status.HTTP_200_OK)



