from django.contrib import admin
from its_app import models
# Register your models here.
admin.site.register(models.Cell)
admin.site.register(models.Data)
admin.site.register(models.StatisticalOverView)