# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2019-11-11 12:40
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('its_app', '0002_auto_20191111_1121'),
    ]

    operations = [
        migrations.CreateModel(
            name='StatisticalOverView',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('std_dev1', models.FloatField(default=0)),
                ('mean', models.FloatField(default=0)),
            ],
        ),
        migrations.RemoveField(
            model_name='cell',
            name='traffic',
        ),
        migrations.AddField(
            model_name='statisticaloverview',
            name='cell',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='its_app.Cell'),
        ),
    ]
