from django.apps import AppConfig


class ItsAppConfig(AppConfig):
    name = 'its_app'
