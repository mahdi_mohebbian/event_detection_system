from django.db import models

# Create your models here.\

# class Data(models.Model):
#     pass


class Cell(models.Model):
    cell_id = models.CharField(max_length=30, null=False)
    lat = models.FloatField(default=0,null=False)
    lon = models.FloatField(default=0,null=False)
    def __str__(self):
        return self.cell_id.__str__()


class Data(models.Model):
    pos_usr_id = models.CharField(max_length=50,null=False)
    pos_time = models.DateField(null=False)
    cell_id = models.ForeignKey(Cell,on_delete=models.CASCADE)

    def __str__(self):
        return "{} - {} - {}".format(self.pos_usr_id.__str__(),self.pos_time.__str__(),self.cell_id.__str__())



class StatisticalOverView(models.Model):
    std_dev1 = models.FloatField(null=False,default=0)
    mean = models.FloatField(null=False,default=0)
    cell = models.ForeignKey(Cell,on_delete=models.CASCADE,unique=True)

    def __str__(self):
        return "{} : {} - {}".format(self.cell.__str__(),self.mean.__str__(),self.std_dev1.__str__())

