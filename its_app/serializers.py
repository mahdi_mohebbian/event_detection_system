from rest_framework import serializers
from . import models


class CellSerializers(serializers.ModelSerializer):
    class Meta:
        model=models.Cell
        fields='__all__'